<?php
echo "Algoritmo 1.";
echo "<br>";
echo "Elabore un algoritmo que permita reordenar un string de adelante hacia atrás, y reemplazando los espacios por alguna palabra ingresada por teclado \n";
echo "<br><br>";

$cadena = "El 31 de diciembre es el ultimo dia del anio";
$palabra ="navidad";
$numero = strlen($cadena)-1;

echo "Frase: ",$cadena;
echo "<br>";
echo "Palabra: ",$palabra;
echo "<br><br>";
echo "Salida:";
echo "<br>";

for($i=$numero; $i>=0; --$i){
    if($cadena[$i]==" "){
        echo $palabra;
    }else
    echo $cadena[$i];
    
}