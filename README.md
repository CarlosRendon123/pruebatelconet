# Prueba practica

## Front-end

### Estilos personzalidos
PRUEBA_PRACTICA/FRONTEND/Views/assets/dist/css/plantilla.css

### Tema
PRUEBA_PRACTICA/FRONTEND/Views/assets/dist/css/adminlte.min.css

### Fuentes utilizadas
https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback
PRUEBA_PRACTICA/FRONTEND/Views/assets/plugins/fontawesome-free/css/all.min.css

### Iconos
https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css

### Alertas
PRUEBA_PRACTICA/FRONTEND/Views/assets/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css

### Bootstrap
https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css

### ESTILOS PARA USO DE DATATABLES JS
https://cdn.datatables.net/1.11.0/css/jquery.dataTables.min.css
https://cdn.datatables.net/responsive/2.2.9/css/responsive.dataTables.min.css
https://cdn.datatables.net/buttons/2.0.0/css/buttons.dataTables.min.css

### scripts 
#### jQuery
PRUEBA_PRACTICA/FRONTEND/Views/assets/plugins/jquery/jquery.min.js
#### Bootstrap 4
PRUEBA_PRACTICA/FRONTEND/Views/assets/plugins/bootstrap/js/bootstrap.bundle.min.js
#### SweetAlert2
PRUEBA_PRACTICA/FRONTEND/Views/assets/plugins/sweetalert2/sweetalert2.min.js
#### JS Bootstrap 5
https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js
####AdminLTE
PRUEBA_PRACTICA/FRONTEND/Views/assets/dist/js/adminlte.min.js
#### DATATABLES
https://cdn.datatables.net/1.11.0/js/jquery.dataTables.min.js
https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js



## back-end
### php puro
Versión de PHP: 7.4.1
### api rest
Para consumir la api rest https://dummyjson.com/users
### Patrón de diseño
Modelo, vista controlador (MVC).
### AJAX

## Base de datos
### My sql
Tipo de servidor: MariaDB
Versión del servidor: 10.4.11-MariaDB - mariadb.org binary distribution