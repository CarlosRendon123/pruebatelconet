-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-01-2023 a las 06:21:32
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `telconet`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `prc_ListarCitas` ()  NO SQL
BEGIN
SELECT c.id,
u.firstname,
u.lastname,
u.email,
c.fecha,
c.especialidad,
c.observaciones
FROM citas c
INNER JOIN users u
on c.user_id = u.id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prc_listarUsers` ()  NO SQL
BEGIN
SELECT  '' as detalles,
		u.id,
        u.firstname,
        u.lastname,
        u.age,
        u.email,
        u.imagen,
        '' as opciones
FROM users u;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `citas`
--

CREATE TABLE `citas` (
  `id` int(11) NOT NULL,
  `fecha` varchar(100) NOT NULL,
  `especialidad` varchar(40) NOT NULL,
  `observaciones` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `citas`
--

INSERT INTO `citas` (`id`, `fecha`, `especialidad`, `observaciones`, `user_id`) VALUES
(3, '20/01/2023', 'Nutricionista', 'ninguna', 14),
(4, '23/03/2023', 'Nutricionista', 'ninguna', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `firstname` varchar(40) NOT NULL,
  `lastname` varchar(40) NOT NULL,
  `age` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `imagen` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `age`, `email`, `imagen`) VALUES
(8, 'Ewell', 'Mueller', 29, 'ggude7@chron.com', 'https://robohash.org/quiaharumsapiente.png'),
(9, 'Demetrius', 'Corkery', 22, 'nloiterton8@aol.com', 'https://robohash.org/excepturiiuremolestiae.png'),
(10, 'Eleanora', 'Price', 37, 'umcgourty9@jalbum.net', 'https://robohash.org/aliquamcumqueiure.png'),
(11, 'Marcel', 'Jones', 39, 'acharlota@liveinternet.ru', 'https://robohash.org/impeditautest.png'),
(12, 'Assunta', 'Rath', 42, 'rhallawellb@dropbox.com', 'https://robohash.org/namquaerataut.png'),
(13, 'Trace', 'Douglas', 26, 'lgribbinc@posterous.com', 'https://robohash.org/voluptatemsintnulla.png'),
(14, 'Enoch', 'Lynch', 21, 'mturleyd@tumblr.com', 'https://robohash.org/quisequienim.png'),
(15, 'Jeanne', 'Halvorson', 26, 'kminchelle@qq.com', 'https://robohash.org/autquiaut.png'),
(16, 'Trycia', 'Fadel', 41, 'dpierrof@vimeo.com', 'https://robohash.org/porronumquamid.png'),
(17, 'Bradford', 'Prohaska', 43, 'vcholdcroftg@ucoz.com', 'https://robohash.org/accusantiumvoluptateseos.png'),
(18, 'Arely', 'Skiles', 42, 'sberminghamh@chron.com', 'https://robohash.org/nihilharumqui.png'),
(19, 'Gust', 'Purdy', 46, 'bleveragei@so-net.ne.jp', 'https://robohash.org/delenitipraesentiumvoluptatum.png'),
(20, 'Lenna', 'Renner', 41, 'aeatockj@psu.edu', 'https://robohash.org/ipsumutofficiis.png'),
(21, 'Doyle', 'Ernser', 23, 'ckensleyk@pen.io', 'https://robohash.org/providenttemporadelectus.png'),
(22, 'Tressa', 'Weber', 41, 'froachel@howstuffworks.com', 'https://robohash.org/temporarecusandaeest.png'),
(23, 'Felicity', 'O\'Reilly', 46, 'beykelhofm@wikispaces.com', 'https://robohash.org/odioquivero.png'),
(24, 'Jocelyn', 'Schuster', 19, 'brickeardn@fema.gov', 'https://robohash.org/odiomolestiaealias.png'),
(25, 'Edwina', 'Ernser', 21, 'dfundello@amazon.co.jp', 'https://robohash.org/doloremautdolores.png'),
(26, 'Griffin', 'Braun', 35, 'lgronaverp@cornell.edu', 'https://robohash.org/laboriosammollitiaut.png'),
(27, 'Piper', 'Schowalter', 47, 'fokillq@amazon.co.jp', 'https://robohash.org/nequeodiosapiente.png'),
(28, 'Kody', 'Terry', 28, 'xisherwoodr@ask.com', 'https://robohash.org/consequunturabnon.png'),
(29, 'Macy', 'Greenfelder', 45, 'jissetts@hostgator.com', 'https://robohash.org/amettemporeea.png'),
(30, 'Maurine', 'Stracke', 31, 'kdulyt@umich.edu', 'https://robohash.org/perferendisideveniet.png');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `citas`
--
ALTER TABLE `citas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `citas`
--
ALTER TABLE `citas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `citas`
--
ALTER TABLE `citas`
  ADD CONSTRAINT `citas_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
