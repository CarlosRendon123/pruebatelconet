 <!-- Content Header (Page header) -->
 <div class="content-header">
     <div class="container-fluid">
         <div class="row mb-2">
             <div class="col-sm-6">
                 <h1 class="m-0">Base de datos</h1>
             </div><!-- /.col -->
             <div class="col-sm-6">
                 <ol class="breadcrumb float-sm-right">
                     <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                     <li class="breadcrumb-item active">Base de datos</li>
                 </ol>
             </div><!-- /.col -->
         </div><!-- /.row -->
     </div><!-- /.container-fluid -->
 </div>
 <!-- /.content-header -->

 <!-- Main content -->
 <div class="content">
     <div class="container-fluid">
         <div class="row">
             <form method="post" enctype="multipart/form-data" id="form_cargar">


                 <button class="btn btn-primary">Llenar</button>
             </form>
         </div>

     </div><!-- /.container-fluid -->
 </div>
 <!-- /.content -->
 <script>
     $(document).ready(function() {

         $("#form_cargar").on('submit', function(e) {
             e.preventDefault();


             $.ajax({
                 url: "PRUEBA_PRACTICA/BACKEND/ajax/base.ajax.php",
                 type: "POST",
                 data: {
                     'accion': 1 // Llenar base de datos
                 },
                 dataType: 'json',
                 success: function(respuesta) {
                     console.log(respuesta);

                     if (respuesta['total'] > 0) {

                         Swal.fire({
                             position: 'center',
                             icon: 'success',
                             title: 'Se cargaron ' + respuesta['total'] + ' registros ',
                             showConfirmButton: false,
                             timer: 2500
                         })

                         $("#btnCargar").prop("disabled", false);
                         $("#img_carga").attr("style", "display:none");
                     } else {

                         Swal.fire({
                             position: 'center',
                             icon: 'error',
                             title: 'Se presento un error al momento de realizar el registro!',
                             showConfirmButton: false,
                             timer: 2500
                         })


                     }

                 }

             });

         })

     })
 </script>