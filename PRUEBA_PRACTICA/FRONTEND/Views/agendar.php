 <!-- Content Header (Page header) -->
 <div class="content-header">
     <div class="container-fluid">
         <div class="row mb-2">
             <div class="col-sm-6">
                 <h1 class="m-0">Agendar cita médica</h1>
             </div><!-- /.col -->
             <div class="col-sm-6">
                 <ol class="breadcrumb float-sm-right">
                     <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                     <li class="breadcrumb-item active">Agendar cita médica</li>
                 </ol>
             </div><!-- /.col -->
         </div><!-- /.row -->
     </div><!-- /.container-fluid -->
 </div>
 <!-- /.content-header -->

 <!-- Main content -->
 <div class="content">
     <div class="container-fluid">
         <div class="row">
            <div class="col-md-6">
            <form method="post" enctype="multipart/form-data" id="form_agendar">
                 <div class="form-row">
                     <div class="form-group col-md-12">
                         <label for="Upgrade">Buscar usuario</label>
                         <select class="form-select" name="sel_user" id="sel_user">
                             <option value="">...</option>
                         </select>
                     </div>
                     <div class="form-group col-md-12">
                         <label for="fecha">fecha y hora</label>
                         <input required="required" type="text" class="form-control" id="fecha" placeholder="Ingrese fecha y hora">
                     </div>


                 </div>
                 <div class="form-row">
                     <div class="form-group col-md-12">
                         <label for="esp">Escoga una especialidad:</label>

                         <select class="form-select" name="esp" id="esp">
                             <option value="">...</option>
                             <option value="Medicina">Medicina General</option>
                             <option value="Nutricionista">Nutricionista</option>
                             <option value="Odontología">Odontología</option>
                         </select>
                     </div>
                     <div class="form-group col-md-12">
                         <label for="obs">Observaciones</label>
                         <textarea required="required" type="text" class="form-control" id="obs" placeholder="Ingrese observaciones"></textarea>
                     </div>
                     <input type="button" value="Agendar" class="btn btn-primary btn-lg"
              style="padding-left: 2.5rem; padding-right: 2.5rem;" id="btnAgendar">

                 </div>
            </div>
            <div class="col-md-5">
            <form method="post" enctype="multipart/form-data">
            <div class="form-row">
                     <div class="form-group col-md-12">
                         <div class="card card-primary">
                             <div class="card-header">
                                 <h3 class="card-title">Datos de usuario</h3>

                             </div>
                             <div class="card-body">
                                 <div class="form-group">
                                     <label id="nombre">Nombre</label><br>
                                     <label id="edad">edad</label><br>
                                     <img id="imagen">

                                 </div>
                             </div>
                             <!-- /.card-body -->
                         </div>
                         <!-- /.card -->
                     </div>
            </div>
             
                 
                 </div>

                 
             </form>
         </div>

     </div><!-- /.container-fluid -->
 </div>
 <!-- /.content -->
 <script>
     var table;
     var idCita;
     var fecha;
     var esp;
     var obs;


     $(document).ready(function() {



         $.ajax({
             url: "PRUEBA_PRACTICA/BACKEND/ajax/users.ajax.php",
             type: "POST",
             data: {
                 'accion': 1 // CONSULTAR EMAILS
             },
             dataType: 'json',
             success: function(respuesta) {

                 var len = respuesta.length;

                 $("#sel_user").empty(); //VACIA EL SELECT
                 
                 for (var i = 0; i < len; i++) {
                     var id = respuesta[i]['id'];
                     var email = respuesta[i]['email'];

                     $("#sel_user").append('<option value="' + email + '">' + email + '</option>');

                 }


             }



         });

         $("#sel_user").change(function() {
             var email = $(this).val();
             /* alert($(this).val()); */
             $.ajax({
                 url: "PRUEBA_PRACTICA/BACKEND/ajax/users.ajax.php",
                 type: "POST",
                 data: {
                     'accion': 1 // Listar usuarios
                 },
                 dataType: 'json',
                 success: function(respuesta) {

                     var len = respuesta.length;


                     for (var i = 0; i < len; i++) { //for para obtener los datoas de usuario por email
                         if (respuesta[i]['email'] == email) {
                             idCita = respuesta[i]['id'];

                             $("#nombre").html('Nombre: ' + respuesta[i]['firstname']);
                             $("#edad").html('edad: ' + respuesta[i]['age']);
                             /* $("#imagen").html('imagen: ' + respuesta[i]['imagen']); */
                             $("#imagen").attr('src', respuesta[i]['imagen']);

                         }

                     }





                 }



             });


         });

         $('#btnAgendar').click(function() {
             fecha = $("#fecha").val();
             /* alert($('select[id=esp]').val()); */
             esp = $("#esp option:selected").text();
             obs = $("#obs").val();

             Swal.fire({
                 title: 'Está seguro de agendar?',
                 icon: 'warning',
                 showCancelButton: true,
                 confirmButtonColor: '#3085d6',
                 cancelButtonColor: '#d33',
                 confirmButtonText: 'Si, deseo agendar',
                 cancelButtonText: 'Cancelar',
             }).then((result) => {
                 if (result.isConfirmed) {
                     console.log(idCita);
                     console.log(fecha);
                     console.log(esp);
                     console.log(obs);

                     var datosCita = new FormData();

                     datosCita.append("accion", 4);
                     datosCita.append("id", idCita);
                     datosCita.append("fecha", fecha);
                     datosCita.append("esp", esp);
                     datosCita.append("obs", obs);


                     $.ajax({
                         url: "PRUEBA_PRACTICA/BACKEND/ajax/users.ajax.php",
                         type: "POST",
                         data: datosCita,
                         cache: false,
                         contentType: false,
                         processData: false,
                         dataType: 'json',
                         success: function(respuesta) {
                             console.log(respuesta);
                             if (respuesta == "ok") {
                                 Swal.fire({
                                     title: 'Se Agendo correctamente',
                                     icon: 'success',
                                     confirmButtonColor: '#3085d6',
                                     confirmButtonText: 'OK',
                                   
                                 })
                                 /* table.reload(); */
                             } else {
                                Swal.fire({
                                     icon: 'error',
                                     title: 'No se pudo agendar cita médica'
                                 });
                             }

                             CargarContenido('PRUEBA_PRACTICA/FRONTEND/Views/dashboard.php','content-wrapper');                         }


                         


                     });

                 }
             });




         })


     })
 </script>