<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a style="cursor: pointer;" class="nav-link active" onclick="CargarContenido('PRUEBA_PRACTICA/FRONTEND/Views/dashboard.php','content-wrapper')">
                Inicio
            </a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a style="cursor: pointer;" class="nav-link" onclick="CargarContenido('PRUEBA_PRACTICA/FRONTEND/Views/ListaCitas.php','content-wrapper')">
                Citas medicas</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a style="cursor: pointer;" class="nav-link" onclick="CargarContenido('PRUEBA_PRACTICA/FRONTEND/Views/ListaUsuarios.php','content-wrapper')">
                Usuarios</a>
        </li>
    </ul>


</nav>
<!-- /.navbar -->