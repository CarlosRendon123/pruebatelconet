 <!-- Main Sidebar Container -->
 <aside class="main-sidebar sidebar-dark-primary elevation-4">
     <!-- Brand Logo -->
     <a href="#" class="brand-link">
         
         <span onclick="CargarContenido('PRUEBA_PRACTICA/FRONTEND/Views/dashboard.php','content-wrapper')" class="brand-text font-weight-light">Agendas médicas</span>
     </a>
     <a href="#" class="brand-link">
         <img src="PRUEBA_PRACTICA/FRONTEND/Views/assets/dist/img/user2.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
         <span onclick="CargarContenido('PRUEBA_PRACTICA/FRONTEND/Views/login.php','content-wrapper')" class="brand-text font-weight-light">Iniciar sesión</span>
     </a>
     

     <!-- Sidebar -->
     <div class="sidebar">

         <!-- Sidebar Menu -->
         <nav class="mt-2">

             <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">

                 <li class="nav-item">
                     <a style="cursor: pointer;" class="nav-link active" onclick="CargarContenido('PRUEBA_PRACTICA/FRONTEND/Views/dashboard.php','content-wrapper')">
                         <i class="nav-icon fas fa-th"></i>
                         <p>
                             Tablero Principal
                         </p>
                     </a>
                 </li>

                 <li class="nav-item">
                     <a href="#" class="nav-link">
                         <i class="nav-icon fas fa-tachometer-alt"></i>
                         <p>
                             Mantenimiento
                             <i class="right fas fa-angle-left"></i>
                         </p>
                     </a>
                     <ul class="nav nav-treeview">
                         <li class="nav-item">
                             <a style="cursor: pointer;" class="nav-link" onclick="CargarContenido('PRUEBA_PRACTICA/FRONTEND/Views/llenarBD.php','content-wrapper')">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>LLenar base de datos</p>
                             </a>
                         </li>
                         <li class="nav-item">
                             <a style="cursor: pointer;" class="nav-link" onclick="CargarContenido('PRUEBA_PRACTICA/FRONTEND/Views/agendar.php','content-wrapper')">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>Agendar citas</p>
                             </a>
                         </li>
                         <li class="nav-item">
                             <a style="cursor: pointer;" class="nav-link" onclick="CargarContenido('PRUEBA_PRACTICA/FRONTEND/Views/cancelar.php','content-wrapper')">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>Cancelar citas</p>
                             </a>
                         </li>
                         <li class="nav-item">
                             <a style="cursor: pointer;" class="nav-link" onclick="CargarContenido('PRUEBA_PRACTICA/FRONTEND/Views/users.php','content-wrapper')">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>Usuarios</p>
                             </a>
                         </li>
                     </ul>
                 </li>
                 <li class="nav-item">
                     <a style="cursor: pointer;" class="nav-link" onclick="CargarContenido('PRUEBA_PRACTICA/FRONTEND/Views/ListaCitas.php','content-wrapper')">
                         <i class="nav-icon fas fa-th"></i>
                         <p>
                             Citas medicas
                         </p>
                     </a>
                 </li>
                 <li class="nav-item">
                     <a style="cursor: pointer;" class="nav-link" onclick="CargarContenido('PRUEBA_PRACTICA/FRONTEND/Views/ListaUsuarios.php','content-wrapper')">
                         <i class="nav-icon fas fa-th"></i>
                         <p>
                             Usuarios
                         </p>
                     </a>
                 </li>
                 
             </ul>
         </nav>
         <!-- /.sidebar-menu -->
     </div>
     <!-- /.sidebar -->
 </aside>

 <script>
     $(".nav-link").on('click', function() {
         $(".nav-link").removeClass('active');
         $(this).addClass('active');
     })
 </script>