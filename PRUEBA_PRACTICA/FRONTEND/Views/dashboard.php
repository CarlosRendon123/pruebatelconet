 <!-- Content Header (Page header) -->
 <div class="content-header">
     <div class="container-fluid">
         <div class="row mb-2">
             <div class="col-sm-6">
                 <h1 class="m-0">Tablero Principal</h1>
             </div><!-- /.col -->
             <div class="col-sm-6">
                 <ol class="breadcrumb float-sm-right">
                     <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                     <li class="breadcrumb-item active">Tablero Principal</li>
                 </ol>
             </div><!-- /.col -->
         </div><!-- /.row -->
     </div><!-- /.container-fluid -->
 </div>
 <!-- /.content-header -->

 <!-- Main content -->
 <div class="content">
     <div class="container-fluid">

         <div class="row">
             <div class="col-md-12">
                <h3>Lista de Citas médicas</h3>
                 
                 <table id="tblUsers2" class="table table-stripped w-100 shadow">
                     <thead>
                         <tr>

                             <th># de cita</th>
                             <th>Nombre</th>
                             <th>Apellido</th>
                             <th>email</th>
                             <th>fecha</th>
                             <th>especialidad</th>
                             <th>observaciones</th>
                         </tr>
                     </thead>
                     <tbody>

                     </tbody>

                 </table>
             </div>
         </div>

     </div><!-- /.container-fluid -->
 </div>
 <!-- /.content -->

 <div class="content">
     <div class="container-fluid">

         <div class="row">
             <div class="col-md-12">
                 <h3>Lista de usuarios</h3>
                 <table id="tblUsers" class="table table-stripped w-100 shadow">
                     <thead>
                         <tr>

                             <th>id</th>
                             <th>Nombre</th>
                             <th>Apellido</th>
                             <th>edad</th>
                             <th>email</th>
                             <th>imagen</th>
                         </tr>
                     </thead>
                     <tbody>

                     </tbody>

                 </table>
             </div>
         </div>

     </div><!-- /.container-fluid -->
 </div>
 <!-- /.content -->
 <script>
     $(document).ready(function() {

         var table;

         $.ajax({
             url: "PRUEBA_PRACTICA/BACKEND/ajax/users.ajax.php",
             type: "POST",
             data: {
                 'accion': 1 //LISTAR USUARUIOS
             },
             dataType: 'json',
             success: function(respuesta) {
                 console.log(respuesta);
                 $.each(respuesta, function(index, value) {
                     $("#tblUsers").append(
                         '<tr>' +

                         '<td id="id">' + value["id"] + '</td>' +
                         '<td>' + value["firstname"] + '</td>' +
                         '<td>' + value["lastname"] + '</td>' +
                         '<td>' + value["age"] + '</td>' +
                         '<td>' + value["email"] + '</td>' +
                         '<td>' + value["imagen"] + '</td>' 

                     )
                 });


             }


         });

         

         $.ajax({
             url: "PRUEBA_PRACTICA/BACKEND/ajax/users.ajax.php",
             type: "POST",
             data: {
                 'accion': 2 // Listar citas
             },
             dataType: 'json',
             success: function(respuesta) {
                 console.log(respuesta);
                 $.each(respuesta, function(index, value) {
                     $("#tblUsers2").append(
                         '<tr>' +

                         '<td id="id">' + value["id"] + '</td>' +
                         '<td>' + value["firstname"] + '</td>' +
                         '<td>' + value["lastname"] + '</td>' +
                         '<td>' + value["email"] + '</td>' +
                         '<td>' + value["fecha"] + '</td>' +
                         '<td>' + value["especialidad"] + '</td>' +
                         '<td>' + value["observaciones"] + '</td>' 

                     )
                 });


             }


         });

         
     })
 </script>

 