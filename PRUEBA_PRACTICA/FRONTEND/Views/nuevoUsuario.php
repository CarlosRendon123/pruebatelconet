
<br>
<div class="row">
<section>
  <div class="container-fluid h-custom">
    <div class="row d-flex justify-content-center align-items-center">
      <!-- <div class="col-md-8 col-lg-6 col-xl-5">
        <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.webp"
          class="img-fluid" alt="Sample image">
      </div> -->
      <div class="col-md-6 col-lg-6 col-xl-4">
        <form>
          <div class="d-flex flex-row align-items-center justify-content-center ">
            <h1 class="m-0">Nuevo usuario</h1>
            
          </div>
          <br>

          <!-- Nombre input -->
          <div class="form-outline mb-4">
            <input required="required" type="text" id="Nombre" class="form-control form-control-lg"
              placeholder="Nombre" />
          </div>

          <!-- Apellido input -->
          <div class="form-outline mb-3">
            <input required="required" type="text" id="Apellido" class="form-control form-control-lg"
              placeholder="Apellido" />
          </div>

          <!-- edad input -->
          <div class="form-outline mb-4">
           <input required="required" type="text" id="edad" class="form-control form-control-lg"
              placeholder="edad" />
          </div> 

          <!-- email input -->
          <div class="form-outline mb-4">
           <input required="required" type="email" id="email" class="form-control form-control-lg"
              placeholder="email" />
          </div> 
          <!-- imagen input -->
          <div class="form-outline mb-3">
            <input required="required" type="test" id="imagen" class="form-control form-control-lg"
              placeholder="imagen" />
          </div>

          

          <div class="text-center text-lg-start mt-4 pt-2">
            <button type="submit" class="btn btn-primary btn-lg"
              style="padding-left: 2.5rem; padding-right: 2.5rem;">Agregar usuario</button>
           
          </div>

        </form>
      </div>
    </div>
  </div>
  
</section>
</div>