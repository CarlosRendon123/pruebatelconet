 <!-- Content Header (Page header) -->
 <div class="content-header">
     <div class="container-fluid">
         <div class="row mb-2">
             <div class="col-sm-6">
                 <h1 class="m-0">Administrar Usuarios</h1>
             </div><!-- /.col -->
             <div class="col-sm-6">
                 <ol class="breadcrumb float-sm-right">
                     <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                     <li class="breadcrumb-item active">Administrar Usuarios</li>
                 </ol>
             </div><!-- /.col -->
         </div><!-- /.row -->
     </div><!-- /.container-fluid -->
 </div>
 <!-- /.content-header -->

 <!-- Main content -->
 <div class="content">
     <div class="container-fluid">

         <div class="row">
             <div class="col-md-12">
                 <form method="post" enctype="multipart/form-data" id="form_cargar">

                     <button type="button" class="btn btn-primary btn-lg" style="padding-left: 2.5rem; padding-right: 2.5rem;" 
                     onclick="CargarContenido('PRUEBA_PRACTICA/FRONTEND/Views/nuevoUsuario.php','content-wrapper')">Agregar Usuario</button>
                 </form><br>
                 <table id="tblUsers" class="table table-stripped w-100 shadow">
                     <thead>
                         <tr>

                             <th>id</th>
                             <th>Nombre</th>
                             <th>Apellido</th>
                             <th>edad</th>
                             <th>email</th>
                             <th>imagen</th>
                             <th>opcion</th>
                         </tr>
                     </thead>
                     <tbody>

                     </tbody>

                 </table>
             </div>
         </div>

     </div><!-- /.container-fluid -->
 </div>
 <!-- /.content -->
 <script>
     $(document).ready(function() {

         var table;

         $.ajax({
             url: "PRUEBA_PRACTICA/BACKEND/ajax/users.ajax.php",
             type: "POST",
             data: {
                 'accion': 1 // Llenar Lista de usuarios
             },
             dataType: 'json',
             success: function(respuesta) {
                 console.log(respuesta);
                 $.each(respuesta, function(index, value) {
                     $("#tblUsers").append(
                         '<tr>' +

                         '<td id="id">' + value["id"] + '</td>' +
                         '<td>' + value["firstname"] + '</td>' +
                         '<td>' + value["lastname"] + '</td>' +
                         '<td>' + value["age"] + '</td>' +
                         '<td>' + value["email"] + '</td>' +
                         '<td>' + value["imagen"] + '</td>' +
                         '<td><center><span class="btnEditar text-primary px-1" style="cursor:pointer;">' +
                         '<i class="fas fa-pencil-alt fs-5"></i></span><span class="btnEliminar text-danger px-1" style="cursor:pointer;">' +
                         '<i class="fas fa-trash-alt fs-5"></i></span></center></td>'

                     )
                 });


             }


         });

         $('#tblUsers tbody').on('click', '.btnEditar', function() {

             accion = 2;


         })

         $('#tblUsers tbody').on('click', '.btnEliminar', function() {

             var accion = 3;
             var id = $(this).parents("tr").find("td")[0].innerHTML;

             Swal.fire({
                 title: 'Recuerde que no puede eliminar usuarios que tengan pendiente citas médicas. Está seguro de eliminar el usuario seleccionado?',
                 icon: 'warning',
                 showCancelButton: true,
                 confirmButtonColor: '#3085d6',
                 cancelButtonColor: '#d33',
                 confirmButtonText: 'Si, deseo eliminarlo',
                 cancelButtonText: 'Cancelar',
             }).then((result) => {
                 if (result.isConfirmed) {
                     var datos = new FormData();

                     datos.append("accion", accion);
                     datos.append("id", id);

                     $.ajax({
                         url: "PRUEBA_PRACTICA/BACKEND/ajax/users.ajax.php",
                         type: "POST",
                         data: datos,
                         cache: false,
                         contentType: false,
                         processData: false,
                         dataType: 'json',
                         success: function(respuesta) {
                             console.log(respuesta);

                             if (respuesta == "ok") {
                                 Swal.fire({
                                     title: 'Se elimino correctamente',
                                     icon: 'success',
                                     confirmButtonColor: '#3085d6',
                                     confirmButtonText: 'OK',
                                   
                                 })
                                 /* table.reload(); */
                             } else {
                                Swal.fire({
                                     icon: 'error',
                                     title: 'El usuario tiene asignada una cita médica. El usuario no se elimino'
                                 });
                             }

                             CargarContenido('PRUEBA_PRACTICA/FRONTEND/Views/users.php','content-wrapper');                         }


                     })

                 }
             })


         })

     })
 </script>