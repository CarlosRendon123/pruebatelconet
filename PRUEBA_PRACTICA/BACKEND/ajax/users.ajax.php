<?php

require_once "../Controllers/users.controlador.php";
require_once "../Models/users.modelo.php";


class ajaxUsers{

    /* public $id; */


    public function ajaxListar(){

        $respuesta = UsersControlador::ctrListar();

        echo json_encode($respuesta);
    }

    public function ajaxEliminar(){
        
      
        $respuesta = UsersControlador::ctrEliminar($_POST['id']);

        echo json_encode($respuesta);
    }

    public function ajaxAgendar(){
        
      
        $respuesta = UsersControlador::ctrAgendar($_POST['id'],$_POST['fecha'],$_POST['esp'],$_POST['obs']);

        echo json_encode($respuesta);
    }
    public function ajaxListarCitas(){

        $respuesta = UsersControlador::ctrListarCitas();

        echo json_encode($respuesta);
    }
    public function ajaxEliminarCita(){
        
      
        $respuesta = UsersControlador::ctrEliminarCita($_POST['id']);

        echo json_encode($respuesta);
    }


}

if(isset($_POST['accion']) && $_POST['accion'] == 1){ //Ejecutar function Listar

    $datos = new ajaxUsers();
    /* var_dump($_POST['accion']);die(); */
    $datos -> ajaxListar();

}else if(isset($_POST['accion']) && $_POST['accion'] == 2){ //Ejecutar function Listar citas

    $datos = new ajaxUsers();
    $datos -> ajaxListarCitas();

}else if(isset($_POST['accion']) && $_POST['accion'] == 3){ //Ejecutar function Eliminar

    $datos = new ajaxUsers();
    $datos -> ajaxEliminar();

}else if(isset($_POST['accion']) && $_POST['accion'] == 4){ //Ejecutar function agendar

    $datos = new ajaxUsers();
    $datos -> ajaxAgendar();

}else if(isset($_POST['accion']) && $_POST['accion'] == 5){ //Ejecutar function Eliminar cita

    $datos = new ajaxUsers();
    $datos -> ajaxEliminarCita();

}