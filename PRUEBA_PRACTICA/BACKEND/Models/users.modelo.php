<?php

require_once "conexion.php";

class UsersModelo
{

    static public function mdlListar() //lISTA DE USUARIOS
    {

        $stmt = Conexion::conectar()->prepare('call prc_listarUsers()');

        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    static public function mdlListarCitas()
    {

        $stmt = Conexion::conectar()->prepare('call prc_listarCitas()');

        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    static public function mdlEliminar($id)
    {


        /* var_dump("DELETE FROM users WHERE id = '$id'");die(); */

        $stmt = Conexion::conectar()->prepare("DELETE FROM users WHERE id = :id");
        $stmt->bindParam(":id", $id, PDO::PARAM_STR);

        if ($stmt->execute()) {
            return "ok";
        } else {
            return Conexion::conectar()->errorInfo();
        }
    }

    static public function mdlAgendar($id, $fecha, $esp, $obs)
    {


        /* var_dump("INSERT INTO citas(fecha,
        especialidad,
        observaciones,
        user_id)
        values('$fecha',
            '$esp',
            '$obs',
            '$id');");die(); */

        $stmt = Conexion::conectar()->prepare("INSERT INTO citas(fecha,
                                                            especialidad,
                                                            observaciones,
                                                            user_id)
                                                            values(:fecha,
                                                                :esp,
                                                                :obs,
                                                                :id);");

        $stmt->bindParam(":fecha", $fecha, PDO::PARAM_STR);
        $stmt->bindParam(":esp", $esp, PDO::PARAM_STR);

        $stmt->bindParam(":obs", $obs, PDO::PARAM_STR);
        $stmt->bindParam(":id", $id, PDO::PARAM_STR);

        if ($stmt->execute()) {
            return "ok";
        } else {
            return Conexion::conectar()->errorInfo();
        }
    }

    static public function mdlEliminarCita($id)
    {


        /* var_dump("DELETE FROM citas WHERE id = '$id'");die(); */

        $stmt = Conexion::conectar()->prepare("DELETE FROM citas WHERE id = :id");
        $stmt->bindParam(":id", $id, PDO::PARAM_STR);

        if ($stmt->execute()) {
            return "ok";
        } else {
            return Conexion::conectar()->errorInfo();
        }
    }
}
