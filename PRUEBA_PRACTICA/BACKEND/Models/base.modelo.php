<?php

require_once "conexion.php";

class BaseModelo
{

    static public function mdlCargaMasiva()
    {
        //utilizacion de la api rest
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://dummyjson.com/users");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res[] = curl_exec($ch);
        /* var_dump($res);die(); */
        $data = json_decode($res[0], true);

        $Registradas = 0;
        $count = 0;

        foreach ($data as $r) {
            if ($count ==1) {
                break;   
            }
            $count++;

            for($i=0;$i<20;$i++){
                

                $firstName = $r[$i]["firstName"];
                $lastName = $r[$i]["lastName"];
                $age = $r[$i]["age"];
                $email =$r[$i]["email"];
                $imagen =$r[$i]["image"];

                /* var_dump("INSERT INTO users(firstname,
                lastname,
                age,
                email,
                imagen)
                values('$firstName',
                        '$lastName',
                        '$age',
                        '$email',
                        '$imagen');");die(); */

                $stmt = Conexion::conectar()->prepare("INSERT INTO users(firstname,
                                                                                    lastname,
                                                                                    age,
                                                                                    email,
                                                                                    imagen)
                                                                        values(:firstName,
                                                                                :lastName,
                                                                                :age,
                                                                                :email,
                                                                                :imagen);");

                    $stmt->bindParam(":firstName", $firstName, PDO::PARAM_STR);
                    $stmt->bindParam(":lastName", $lastName, PDO::PARAM_STR);
                    $stmt->bindParam(":age", $age, PDO::PARAM_STR);

                    $stmt->bindParam(":email", $email, PDO::PARAM_STR);
                    $stmt->bindParam(":imagen", $imagen, PDO::PARAM_STR);

                    if ($stmt->execute()) {
                        $Registradas = $Registradas + 1;
                        
                    } else {
                        $Registradas = 0;
                    }

                    
            }

        }
        /* var_dump($Registradas);die(); */
        $respuesta["total"] = $Registradas;

        return $respuesta;
    }
}
