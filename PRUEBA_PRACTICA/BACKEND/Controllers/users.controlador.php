<?php


class UsersControlador{

    static public function ctrListar(){
        
        $respuesta = UsersModelo::mdlListar();
        
        return $respuesta;
    }

    static public function ctrEliminar($id){
        
        /* var_dump($id);die(); */
        
        $respuesta = UsersModelo::mdlEliminar($id);
        
        
        return $respuesta;
    }
    static public function ctrAgendar($id,$fecha,$esp,$obs){
        
        /* var_dump($id);die(); */
        
        $respuesta = UsersModelo::mdlAgendar($id,$fecha,$esp,$obs);
        
        
        return $respuesta;
    }
    static public function ctrListarCitas(){
        
        $respuesta = UsersModelo::mdlListarCitas();
        
        return $respuesta;
    }
    static public function ctrEliminarCita($id){
        
        /* var_dump($id);die(); */
        
        $respuesta = UsersModelo::mdlEliminarCita($id);
        
        
        return $respuesta;
    }
}